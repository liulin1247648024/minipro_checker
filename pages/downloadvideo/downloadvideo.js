// pages/downloadvideo.js

//获取应用实例
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    tips: '',
    tips: '视频保存成功，可前往相册查看'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var docId = options.docId;
    if (!docId) {
      var scene = decodeURIComponent(options.scene)
      var sceneArray = scene.split("=");

      var sceneParams = {};
      for (var i = 0; i < sceneArray.length; i += 2) {
        sceneParams[sceneArray[i]] = sceneArray[i + 1]
      }

      if (sceneParams["docId"]) {
        docId = sceneParams["docId"];
      }
    }
    console.log("docId ==== ", docId);
    if (docId) {
      this.generateAndDownloadVideo(docId);
    } else {
      this.setData({
        tips: "参数错误，docId不能为空"
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 下载视频到相册
   */
  generateAndDownloadVideo: function (docId) {
    let _this = this;
    console.log("generateAndDownloadVideo  docId === ", docId);
    // docId = 146;
    // 调用checker生成文档视频并保存到相册
    var url = app.globalData.checkerUrl + "/html/generateVideo?docId=" + docId;
    console.log("url === ", url);
    wx.showLoading({ title: '视频下载中'});
    var filePath = wx.env.USER_DATA_PATH + '/' + new Date().getTime() + '.mp4'
    wx.downloadFile({
      url: url,
      filePath: filePath,
      success: function (res) {
        console.log("downloadFile   success -- res ", res);
        wx.showLoading({ title: '视频保存中' });
        wx.saveVideoToPhotosAlbum({
          // filePath: res.tempFilePath,
          filePath: filePath,
          success: function (res) {
            console.log("saveVideoToPhotosAlbum   success -- res ", res);
            wx.hideLoading();
            wx.showToast({
              title: '视频保存成功！',
              icon: 'success',
              duration: 1500
            })
            _this.setData({
              tips: "视频保存成功，可前往相册查看"
            })
          },
          fail: function (err) {
            console.log("saveVideoToPhotosAlbum   fail err === ", err);
            wx.hideLoading();
            wx.showToast({
              title: '视频保存失败！',
              icon: 'none',
              duration: 1500
            })
            _this.setData({
              tips: "视频保存失败！"
            })
          }
        })
      },
      fail: function (err) {
        console.log("downloadFile   fail -- err ", err);
        wx.hideLoading();
        wx.showToast({
          title: '视频下载失败！',
          icon: 'none',
          duration: 1500
        })
        _this.setData({
          tips: "视频下载失败！"
        })
      }
    });

  }

})