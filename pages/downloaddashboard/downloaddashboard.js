// pages/downloaddashboard/downloaddashboard.js
const util = require('../../utils/util.js')

const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight: 0,
    screenWidth: 0,
    screenHeight: 0,
    canvasWidth: 0,
    canvasHeight: 0,
    canvasWidthScale: 1,
    canvasHeightScale: 1,
    articlePerformance: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var ddk = options.ddk;
    if (!ddk) {
      var scene = decodeURIComponent(options.scene)
      var sceneArray = scene.split("=");

      var sceneParams = {};
      for (var i = 0; i < sceneArray.length; i += 2) {
        sceneParams[sceneArray[i]] = sceneArray[i + 1]
      }

      if (sceneParams["ddk"]) {
        ddk = sceneParams["ddk"];
      }
    }
    console.log("ddk ==== ", ddk);
    if (ddk) {
      let that = this;
      wx.getSystemInfo({
        success(res) {
          var screenWidth = res.screenWidth;
          var screenHeight = res.screenHeight;
          if (screenWidth > screenHeight) {
            screenWidth = res.screenHeight;
            screenHeight = res.screenWidth;
          }
          var canvasHeight = screenWidth * 610 / 320;
          that.setData({
            statusBarHeight: res.statusBarHeight,
            screenWidth: screenWidth,
            screenHeight: screenHeight,
            canvasWidth: screenWidth,
            canvasHeight: canvasHeight,
            canvasWidthScale: screenWidth / 320,
            canvasHeightScale: canvasHeight / 498
          });
          that.downloadDashboardImages(ddk);
        }
      });
    } else {
      wx.showToast({
        title: '参数错误！',
        icon: 'none',
        duration: 1500
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击分享
   */
  onShareAppMessage: function (res) {
    return {
      title: '',
      path: 'pages/downloaddashboard/downloaddashboard?ddk=null438',
      success: function (res) {
        //转发成功
        console.log("转发成功   res === ", res)
      },
      fail: function (res) {
        //转发失败
        console.log("转发失败   res === ", res)
      }
    }
  },

  downloadDashboardImages: function (ddk) {
    let _this = this;
    var dashboardDownloadUrl = app.globalData.checkerUrl + "/html/getDocDashboardEchartsImages?docDashboardImagesKey=" + ddk;
    console.log('dashboardDownloadUrl ==== ', dashboardDownloadUrl);
    wx.showLoading({ title: '正在下载图片资源' });
    wx.request({
      url: dashboardDownloadUrl,
      method: 'GET',
      success: function (res) {
        wx.hideLoading();
        console.log("downloadDashboardImages  res === ", res);
        _this.setData({
          articlePerformance: res.data.articlePerformance
        });
        var echartImagePaths = [];
        var fileManager = wx.getFileSystemManager();
        for (var i = 0; i < res.data.images.length; i++) {
          var tempFilePath = wx.env.USER_DATA_PATH + '/docEchartImage' + i + '.png';
          fileManager.writeFile({
            filePath: tempFilePath,
            data: res.data.images[i].slice("data:image/png;base64,".length),
            encoding: 'base64',
            success: res => {
              echartImagePaths.push(tempFilePath);
              if (echartImagePaths.length == 5) {
                wx.hideLoading();
                _this.generateCanvas();
              }
            },
            fail: err => {
              wx.hideLoading();
              wx.showToast({
                title: '图片保存到临时文件失败！',
                icon: 'none',
                duration: 1500
              })
            }
          });
        }
      },
      fail: function (error) {
        wx.hideLoading();
        util.showNetErrorModel();
      }
    })
  },

  generateCanvas: function () {
    console.log("generateCanvas =============== ");
    let _this = this;
    console.log("articlePerformance  ==== ", _this.data.articlePerformance);
    const ctx = wx.createCanvasContext('shareCanvas');

    var articlePerformance = _this.data.articlePerformance;
    var canvasHeight = _this.data.canvasHeight;
    var canvasWidth = _this.data.canvasWidth;
    var widthScale = _this.data.canvasWidthScale;
    var heightScale = _this.data.canvasHeightScale;
    console.log("canvasHeight === ", canvasHeight);
    console.log("canvasWidth === ", canvasWidth);
    console.log("widthScale === ", widthScale);
    console.log("heightScale === ", heightScale);
    ctx.setFillStyle('#fff');
    ctx.fillRect(0, 0, canvasWidth, canvasHeight);

    // 易读性矩形框
    var readabilityRectTop = 10 * widthScale;
    var readabilityRectleft = 10 * widthScale;
    var readabilityRectWidth = canvasWidth - 2 * readabilityRectleft;
    var readabilityRectHeight = 120 * widthScale;
    ctx.drawImage('../../images/roundRectShadow.png', readabilityRectleft, readabilityRectTop, readabilityRectWidth, readabilityRectHeight);

    var readabilityImgPath = wx.env.USER_DATA_PATH + '/docEchartImage0.png';
    console.log("readabilityImgPath === ", readabilityImgPath);
    var readabilityImgWidth = 100 * widthScale;
    var readabilityImgHeight = 100 * widthScale;
    var readabilityImgLeft = (canvasWidth - 220 * widthScale) / 2;
    var readabilityImgTop = 20 * widthScale;
    ctx.drawImage(readabilityImgPath, readabilityImgLeft, readabilityImgTop, readabilityImgWidth, readabilityImgHeight);

    ctx.setTextAlign('left');
    ctx.setFillStyle('#000');
    ctx.setFontSize(14 * widthScale);
    ctx.fillText('阅读时间：' + articlePerformance.reading, 160 * widthScale, 58 * widthScale, 100 * widthScale);
    ctx.fillText('朗读时间：' + articlePerformance.speaking, 160 * widthScale, 88 * widthScale, 100 * widthScale);

    // 情感矩形框
    var emotionRectTop = readabilityRectTop + readabilityRectHeight + 10 * widthScale;
    var emotionRectleft = 10 * widthScale;
    var emotionRectWidth = canvasWidth - 2 * emotionRectleft;
    var emotionRectHeight = 230 * widthScale;
    ctx.drawImage('../../images/roundRectShadow.png', emotionRectleft, emotionRectTop, emotionRectWidth, emotionRectHeight);


    var posNegEmotionImgPath = wx.env.USER_DATA_PATH + '/docEchartImage1.png';
    console.log("posNegEmotionImgPath === ", posNegEmotionImgPath);
    var posNegEmotionImgWidth = 100 * widthScale;
    var posNegEmotionImgHeight = 100 * widthScale;
    var posNegEmotionImgLeft = (canvasWidth - 220 * widthScale) / 2;
    var posNegEmotionImgTop = emotionRectTop + 10 * widthScale;
    ctx.drawImage(posNegEmotionImgPath, posNegEmotionImgLeft, posNegEmotionImgTop, posNegEmotionImgWidth, posNegEmotionImgHeight);

    var emotionIntensityImgPath = wx.env.USER_DATA_PATH + '/docEchartImage2.png';
    console.log("emotionIntensityImgPath === ", emotionIntensityImgPath);
    var emotionIntensityImgWidth = 100 * widthScale;
    var emotionIntensityImgHeight = 100 * widthScale;
    var emotionIntensityImgLeft = posNegEmotionImgLeft + posNegEmotionImgWidth + 20 * widthScale;
    var emotionIntensityImgTop = emotionRectTop + 10 * widthScale;
    ctx.drawImage(emotionIntensityImgPath, emotionIntensityImgLeft, emotionIntensityImgTop, emotionIntensityImgWidth, emotionIntensityImgHeight);

    var emotionLineImgPath = wx.env.USER_DATA_PATH + '/docEchartImage3.png';
    console.log("emotionLineImgPath === ", emotionLineImgPath);
    var emotionLineImgWidth = 280 * widthScale;
    var emotionLineImgHeight = 110 * widthScale;
    var emotionLineImgLeft = 20 * widthScale;
    var emotionLineImgTop = emotionIntensityImgTop + emotionIntensityImgHeight;
    ctx.drawImage(emotionLineImgPath, emotionLineImgLeft, emotionLineImgTop, emotionLineImgWidth, emotionLineImgHeight);

    // 词统计条形图矩形框
    var wordsStatisticsTop = emotionRectTop + emotionRectHeight + 10 * widthScale;
    var wordsStatisticsleft = 10 * widthScale;
    var wordsStatisticsWidth = canvasWidth - 2 * wordsStatisticsleft;
    var wordsStatisticsHeight = 130 * widthScale;
    ctx.drawImage('../../images/roundRectShadow.png', wordsStatisticsleft, wordsStatisticsTop, wordsStatisticsWidth, wordsStatisticsHeight);


    var wordsStatisticsBarImgPath = wx.env.USER_DATA_PATH + '/docEchartImage4.png';
    console.log("wordsStatisticsBarImgPath === ", wordsStatisticsBarImgPath);
    var wordsStatisticsBarImgLeft = 15 * widthScale;
    var wordsStatisticsBarImgWidth = canvasWidth - wordsStatisticsBarImgLeft * 2;
    var wordsStatisticsBarImgHeight = 130 * widthScale;
    var wordsStatisticsBarImgTop = wordsStatisticsTop;
    ctx.drawImage(wordsStatisticsBarImgPath, wordsStatisticsBarImgLeft, wordsStatisticsBarImgTop, wordsStatisticsBarImgWidth, wordsStatisticsBarImgHeight);

    // 词统计数据矩形框
    var wordsStatisticsTextRectTop = wordsStatisticsTop + wordsStatisticsHeight + 10 * widthScale;
    var wordsStatisticsTextRectleft = 10 * widthScale;
    var wordsStatisticsTextRectWidth = canvasWidth - 2 * wordsStatisticsTextRectleft;
    var wordsStatisticsTextRectHeight = 35 * widthScale;
    ctx.drawImage('../../images/roundRectShadow.png', wordsStatisticsTextRectleft, wordsStatisticsTextRectTop, wordsStatisticsTextRectWidth, wordsStatisticsTextRectHeight);

    ctx.setTextAlign('left');
    ctx.setFillStyle('#000');
    var wordsStatisticsTitleFontSize = 12 * widthScale;
    var wordsStatisticsNumberFontSize = 14 * widthScale;
    var fontSizeDiff = wordsStatisticsNumberFontSize - wordsStatisticsTitleFontSize;
    ctx.setFontSize(wordsStatisticsTitleFontSize);
    var wordsStatisticsColumnSpace = (wordsStatisticsBarImgHeight - wordsStatisticsTitleFontSize * 4.5 - wordsStatisticsNumberFontSize * 3 - 30 * widthScale) / 2;

    var wordsStatisticsTextleft = 30 * widthScale;
    var wordsStatisticsTextTop = wordsStatisticsTextRectTop + 20 * widthScale;
    _this.fillRoundRect(ctx, wordsStatisticsTextleft, wordsStatisticsTextTop - wordsStatisticsTitleFontSize, wordsStatisticsTitleFontSize * 0.5, wordsStatisticsTitleFontSize * 1.3, 2, "#FFC063");

    ctx.fillText('字数', wordsStatisticsTextleft + wordsStatisticsTitleFontSize * 0.8, wordsStatisticsTextTop);

    wordsStatisticsTextleft += wordsStatisticsTitleFontSize * 3.1;
    ctx.font = 'bold ' + wordsStatisticsNumberFontSize + 'px sans-serif';
    ctx.fillText(articlePerformance.characterCount, wordsStatisticsTextleft, wordsStatisticsTextTop + fontSizeDiff / 2);

    // 计算词数文本长度
    ctx.font = 'bold ' + wordsStatisticsNumberFontSize + 'px sans-serif';
    var wordCountValueWidth = ctx.measureText(articlePerformance.wordCount).width;
    var wordCountRegion = wordCountValueWidth + wordsStatisticsTitleFontSize * 3.1;

    wordsStatisticsTextleft = canvasWidth / 2 - wordCountRegion / 2;
    _this.fillRoundRect(ctx, wordsStatisticsTextleft, wordsStatisticsTextTop - wordsStatisticsTitleFontSize, wordsStatisticsTitleFontSize * 0.5, wordsStatisticsTitleFontSize * 1.3, 2, "#977AFF");

    ctx.font = 'normal ' + wordsStatisticsTitleFontSize + 'px sans-serif';
    ctx.fillText('词数', wordsStatisticsTextleft + wordsStatisticsTitleFontSize * 0.8, wordsStatisticsTextTop);

    wordsStatisticsTextleft += wordsStatisticsTitleFontSize * 3.1;
    ctx.font = 'bold ' + wordsStatisticsNumberFontSize + 'px sans-serif';
    ctx.fillText(articlePerformance.wordCount, wordsStatisticsTextleft, wordsStatisticsTextTop + fontSizeDiff / 2);

    // 计算词数文本长度
    var sentenceCountValueWidth = ctx.measureText(articlePerformance.sentenceCount).width;
    var sentenceCountRegion = sentenceCountValueWidth + wordsStatisticsTitleFontSize * 3.1;

    wordsStatisticsTextleft = canvasWidth - 30 - sentenceCountRegion;
    _this.fillRoundRect(ctx, wordsStatisticsTextleft, wordsStatisticsTextTop - wordsStatisticsTitleFontSize, wordsStatisticsTitleFontSize * 0.5, wordsStatisticsTitleFontSize * 1.3, 2, "#FF7C7A");
    ctx.font = 'normal ' + wordsStatisticsTitleFontSize + 'px sans-serif';
    ctx.fillText('句数', wordsStatisticsTextleft + wordsStatisticsTitleFontSize * 0.8, wordsStatisticsTextTop);

    wordsStatisticsTextleft += wordsStatisticsTitleFontSize * 3.1;
    ctx.font = 'bold ' + wordsStatisticsNumberFontSize + 'px sans-serif';
    ctx.fillText(articlePerformance.sentenceCount, wordsStatisticsTextleft, wordsStatisticsTextTop + fontSizeDiff / 2);

    ctx.draw();

  },

  /**该方法用来绘制一个有填充色的圆角矩形 
   *@param cxt:canvas的上下文环境 
    *@param x:左上角x轴坐标 
    *@param y:左上角y轴坐标 
    *@param width:矩形的宽度 
    *@param height:矩形的高度 
    *@param radius:圆的半径 
    *@param fillColor:填充颜色 
  **/ 
  fillRoundRect: function (cxt, x, y, width, height, radius, fillColor) {
    let that = this;
    //圆的直径必然要小于矩形的宽高          
    if (2 * radius > width || 2 * radius > height) { return false; }

    cxt.save();
    cxt.translate(x, y);
    //绘制圆角矩形的各个边  
    that.drawRoundRectPath(cxt, width, height, radius);
    cxt.fillStyle = fillColor || "#000"; //若是给定了值就用给定的值否则给予默认值  
    cxt.fill();
    cxt.restore();
  },

  drawRoundRectPath: function (cxt, width, height, radius) {
    cxt.beginPath(0);
    //从右下角顺时针绘制，弧度从0到1/2PI  
    cxt.arc(width - radius, height - radius, radius, 0, Math.PI / 2);

    //矩形下边线  
    cxt.lineTo(radius, height);

    //左下角圆弧，弧度从1/2PI到PI  
    cxt.arc(radius, height - radius, radius, Math.PI / 2, Math.PI);

    //矩形左边线  
    cxt.lineTo(0, radius);

    //左上角圆弧，弧度从PI到3/2PI  
    cxt.arc(radius, radius, radius, Math.PI, Math.PI * 3 / 2);

    //上边线  
    cxt.lineTo(width - radius, 0);

    //右上角圆弧  
    cxt.arc(width - radius, radius, radius, Math.PI * 3 / 2, Math.PI * 2);

    //右边线  
    cxt.lineTo(width, height - radius);

    cxt.closePath();
  },

  saveImg: function () {
    let that = this;
    // 保存到相册
    wx.canvasToTempFilePath({
      canvasId: 'shareCanvas',
      success: function (res) {
        wx.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
          success: function (res) {
            wx.showToast({
              title: '图片保存成功'
            })
          },
          fail: function (res) {
            if (res.errMsg === "saveImageToPhotosAlbum:fail auth deny" || res.errMsg === "saveImageToPhotosAlbum:fail:auth denied") {
              console.log("用户拒绝相册授权");
            }
            wx.showToast({
              title: '图片保存失败！',
              icon: 'none',
              duration: 1500
            })
          }
        })
      }
    }, this)
  },

})